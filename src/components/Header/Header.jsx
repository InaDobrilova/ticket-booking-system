import React from 'react'
import styled from 'styled-components'

const AppHeader = styled.header`
background-color: #282c34;
min-height: 8vh;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
font-size: calc(10px + 2vmin);
color: white;
`
const Header = () => {
  return (
        <AppHeader>
            <p>Hypoport Booking System</p>
        </AppHeader>
  )
}

export default Header
