import React, { useEffect, useState } from 'react'
import { getAirports } from '../../requests/airports'
import BookingsList from '../Bookings/BookingList/BookingList'
import Form from '../Form/Form'

const AirportsContext = React.createContext({})

const Home = () => {
  const [airports, setAirports] = useState([])
  const [loading, setLoading] = useState(true)
  const [refreshBookingList, setRefreshBookingList] = useState(false)

  const updateRefreshBookingList = () => {
    setRefreshBookingList(!refreshBookingList)
  }

  useEffect(() => {
    getAirports()
      .then(r => {
        setAirports(r)
        setLoading(false)
      })
  }, [])
  return (
    <>
      <AirportsContext.Provider value={{ loading, airports }}>
        <Form updateRefreshBookingList={updateRefreshBookingList}/>
        <BookingsList updateRefreshBookingList={updateRefreshBookingList} refreshBookingList={refreshBookingList}/>
      </AirportsContext.Provider>
    </>
  )
}

export default Home

export {
  AirportsContext
}
