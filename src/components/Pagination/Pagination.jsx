import React from 'react'
import { PAGE_SIZE_MAX, PAGE_SIZE_MID, PAGE_SIZE_MIN } from '../../common/constants'
import styled from 'styled-components'
import { PrimaryButton } from '../Styled/Styled'

const PageSelect = styled.select`
    cursor: pointer;
    user-select: none;
    border: 0;
    height: 1.15em;
    margin: 0;
    display: block;
    padding: 6px 0 7px;
    box-sizing: content-box;
    border: 1px solid black;
    font-size: 14px;
    border-radius: 6px;
    display: inline-block;
    margin-left: 8px;
`
const Pager = styled.div`
    margin: 20px;
`

const ButtonWraper = styled.div`
    margin-top: 20px;
    display: flex;
    justify-content: space-between;
`

const Pagination = ({ paging, onPageSizeChange, onPrevious, onNext, total }) => {
  const hasNext = () => {
    const result = (paging.currentPage + 1) * paging.pageSize < total
    return result
  }

  const hasPrevious = () => {
    const result = paging.currentPage > 0
    return result
  }
  return (
        <>
         <Pager>
              <label htmlFor="page-size">Bookings per page</label>
              <PageSelect id="page-size" onChange={onPageSizeChange}>
                  <option>{PAGE_SIZE_MIN}</option>
                  <option>{PAGE_SIZE_MID}</option>
                  <option>{PAGE_SIZE_MAX}</option>
              </PageSelect>
              <ButtonWraper>
                {hasPrevious() ? <PrimaryButton onClick={onPrevious}>Previous</PrimaryButton> : <PrimaryButton disabled>Previous</PrimaryButton>}
                {hasNext() ? <PrimaryButton onClick={onNext}>Next</PrimaryButton> : <PrimaryButton disabled>Next</PrimaryButton>}
                </ButtonWraper>
           </Pager>

        </>
  )
}

export default Pagination
