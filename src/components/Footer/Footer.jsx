import React from 'react'
import styled from 'styled-components'

const AppFooter = styled.footer`
  background-color: #282c34;
  min-height: 8vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 1vmin);
  color: white;
`

const Footer = () => {
  return (
        <AppFooter>
            {'Copyright © '} All rights reserved
        </AppFooter>
  )
}

export default Footer
