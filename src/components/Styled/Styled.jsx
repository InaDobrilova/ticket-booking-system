
import styled, { css } from 'styled-components'

export const FormRow = styled.div`
    display: flex;
    align-items: baseline;
    justify-content: space-between;
    margin-top: 20px;
`
export const Select = styled.select`
    cursor: pointer;
    user-select: none;
    border: 0;
    height: 1.15em;
    margin: 0;
    display: block;
    padding: 6px 0 7px;
    box-sizing: content-box;
    border: 1px solid black;
    font-size: 14px;
    width: 200px;
    border-radius: 6px;
`

export const PrimaryButton = styled.button`
    background-color: #fff;
    height: 2.5em;
    color: #086788;
    border: 1px solid #086788;
    border-radius: 6px;
    text-align: center;
    margin-top: 20px;
    width: 100px;
    cursor: pointer;
    ${props =>
        props.disabled &&
        css`
        color: grey;
        border-color: grey;
        cursor: default;
      `};
`
