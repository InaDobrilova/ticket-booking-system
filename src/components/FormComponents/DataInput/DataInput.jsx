import React, { useState } from 'react'
import styled, { css } from 'styled-components'
import { FormRow } from '../../Styled/Styled'

const Input = styled.input`
    border: 1px solid #333;
    border-radius: 3px;
    height: 1.5em;
    margin: 0;
    display: block;
    padding: 6px 7px 7px;
    box-sizing: content-box;
    display: inline-block;
    width: 185px;
    ${props =>
      props.error &&
      css`
      ::placeholder,
      ::-webkit-input-placeholder {
        color: red;
      }
      :-ms-input-placeholder {
        color: red;
      }
    `};
    
`

const ErrorSpan = styled.span`
  margin-left: 15px;
  font-size: 12px;
  color: red;
`

const DataInput = ({ onInputChange, formDetails }) => {
  const [nameError, setNameError] = useState({
    firstName: false,
    lastName: false
  })
  const validateName = (key) => {
    const hasError = formDetails[key] === ''
    setNameError({
      nameError,
      [key]: hasError
    })
  }

  const getFirstNamePlaceholder = () => {
    if (nameError.firstName) {
      return 'Please include first name'
    }
    return 'Enter your first name'
  }

  const getLastNamePlaceholder = () => {
    if (nameError.lastName) {
      return 'Please include last name'
    }
    return 'Enter your last name'
  }

  return (
        <>
            <FormRow>
                <label htmlFor="fName">First Name: </label>
                <Input type="text" error={nameError.firstName} id="fName" name="fName" value={formDetails.firstName} placeholder={getFirstNamePlaceholder()}
                onChange={(e) => onInputChange(e, 'firstName')}
                onBlur={() => validateName('firstName')}
                />
            </FormRow>
            <FormRow>
                <label htmlFor="lName">Last Name: </label>
                <Input type="text" error={nameError.lastName} id="lName" name="lName" value={formDetails.lastName} placeholder={getLastNamePlaceholder()}
                onChange={(e) => onInputChange(e, 'lastName')}
                onBlur={() => validateName('lastName')}
                />
            </FormRow>
        </>
  )
}

export default DataInput
