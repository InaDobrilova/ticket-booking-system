import React from 'react'
import { FormRow, Select } from '../../Styled/Styled'

const AirportsList = ({ onInputChange, airports, formDetails }) => {
  return (
      <>
        <FormRow>
            <label htmlFor="departure-airport">Departure airport:</label>
            <Select id="departure-airport" onChange={(e) => onInputChange(e, 'departureAirport')} value={formDetails.departureAirport}>
                <option value="" selected disabled>Choose an airport</option>

                {airports.map((airport) => <option key={airport.id} value={airport.id}>{airport.title}</option>)}
            </Select>
        </FormRow>
        <FormRow>
            <label htmlFor="destination-airport">Destination airport:</label>
            <Select id="destination-airport" onChange={(e) => onInputChange(e, 'destinationAirport')} value={formDetails.destinationAirport}>
                <option value="" selected disabled>Choose an airport</option>

                {airports.map((airport) => <option key={airport.id} value={airport.id}>{airport.title}</option>)}
            </Select>
        </FormRow>
        </>
  )
}

export default AirportsList
