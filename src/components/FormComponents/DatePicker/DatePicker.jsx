import React from 'react'
import { TODAY } from '../../../common/constants'
import { FormRow } from '../../Styled/Styled'
import styled from 'styled-components'

const DateInput = styled.input`
  width: 195px;
  height: 2.5em;
  border-radius: 3px;
  padding-left: 5px;
  border: 1px solid black;
`

const DatePicker = ({ onInputChange, formDetails }) => {
  return (
        <>
           <FormRow>
            <label htmlFor="departureDate">Departure date:</label>
            <DateInput type="date" name="departureDate" id="departureDate" value={formDetails.departureDate} min={TODAY} onChange={(e) => onInputChange(e, 'departureDate')}/>
        </FormRow>
        <FormRow>
            <label htmlFor="returnDate">Return date:</label>
            <DateInput type="date" name="returnDate" id="returnDate" value={formDetails.returnDate} min={formDetails.departureDate} onChange={(e) => onInputChange(e, 'returnDate')}/>
        </FormRow>
        </>
  )
}

export default DatePicker
