import React, { useContext, useState } from 'react'
import { TODAY } from '../../common/constants'
import { bookTicket } from '../../requests/bookings'
import AirportsList from '../FormComponents/AirportsList/AirportsList'
import DataInput from '../FormComponents/DataInput/DataInput'
import DatePicker from '../FormComponents/DatePicker/DatePicker'
import { AirportsContext } from '../Home/Home'
import styled from 'styled-components'
import { PrimaryButton } from '../Styled/Styled'

const StyledForm = styled.form`
  width: 40%;
  margin: 20px auto;
  padding: 20px;
  border-radius: 6px;
  border: 1px solid #086788;
`
const Header = styled.h1`
  text-align: center;
`

const SubmitButtonWrapper = styled.div`
  text-align: center;
`

const Form = ({ updateRefreshBookingList }) => {
  const [formDetails, setFormDetails] = useState({
    firstName: '',
    lastName: '',
    departureAirport: '',
    destinationAirport: '',
    departureDate: TODAY,
    returnDate: TODAY
  })

  const { loading, airports } = useContext(AirportsContext)
  const onInputChange = (e, key) => {
    let value = e.target.value

    if (key === 'departureDate' || key === 'returnDate') {
      const date = new Date(e.target.value)
      value = date.toISOString().substring(0, 10)
    }

    if (key === 'departureAirport' || key === 'destinationAirport') {
      value = parseInt(value, 10)
    }
    formDetails[key] = value
    setFormDetails({ ...formDetails })
  }

  const isFormValid = () => {
    if (formDetails.firstName === '' ||
      formDetails.lastName === '' ||
      formDetails.departureAirport === '' ||
      formDetails.destinationAirport === ''
    ) {
      return false
    }

    return true
  }

  const handleBookingClick = (e) => {
    e.preventDefault()
    bookTicket(formDetails)
      .then(r => {
        updateRefreshBookingList()
        setFormDetails({
          ...formDetails,
          firstName: '',
          lastName: '',
          departureAirport: 0,
          destinationAirport: 0,
          departureDate: TODAY,
          returnDate: TODAY
        })
      })
  }

  if (!loading) {
    return (
      <StyledForm>
        <Header>Book your ticket</Header>
        <DataInput onInputChange={onInputChange} formDetails={formDetails}/>
        <AirportsList onInputChange={onInputChange} airports={airports} formDetails={formDetails}/>
        <DatePicker onInputChange={onInputChange} formDetails={formDetails}/>
        <SubmitButtonWrapper>
          <PrimaryButton disabled={!isFormValid()} type='submit' onClick={handleBookingClick}>Book now</PrimaryButton>

        </SubmitButtonWrapper>
      </StyledForm>
    )
  }

  return (
   <>
    <p>Loading</p>
   </>
  )
}

export default Form
