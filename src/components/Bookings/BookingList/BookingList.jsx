import React, { useContext, useEffect, useState } from 'react'
import { DEFAULT_PAGE, PAGE_SIZE_MIN } from '../../../common/constants'
import { getAllBookings } from '../../../requests/bookings'
import { AirportsContext } from '../../Home/Home'
import SingleBooking from '../SingleBooking/SingleBooking'
import styled from 'styled-components'
import Pagination from '../../Pagination/Pagination'

const BookingHeader = styled.p`
  font-size: 24px;
`

const BookingWrapper = styled.div`
  width: 40%;
  margin: 0 auto;
`

const BookingsList = ({ updateRefreshBookingList, refreshBookingList }) => {
  const [bookings, setBookings] = useState({
    list: [],
    totalCount: 0
  })

  const [paging, setPaging] = useState({
    pageSize: PAGE_SIZE_MIN,
    currentPage: DEFAULT_PAGE
  })

  const { loading, airports } = useContext(AirportsContext)

  useEffect(() => {
    getAllBookings(paging.pageSize, paging.currentPage)
      .then(r => {
        setBookings({ list: r.list, totalCount: parseInt(r.totalCount, 10) })
      })
  }, [paging, refreshBookingList])

  const onPageSizeChange = (e) => {
    const value = parseInt(e.target.value)
    setPaging({
      ...paging,
      pageSize: value
    })
  }

  const onNext = () => {
    setPaging({
      ...paging,
      currentPage: paging.currentPage + 1
    })
  }

  const onPrevious = () => {
    setPaging({
      ...paging,
      currentPage: paging.currentPage - 1
    })
  }

  return (
        <BookingWrapper>
            <BookingHeader>Your bookings:</BookingHeader>
            {loading ? <></> : bookings.list.map(booking => <SingleBooking airports={airports} booking={booking} key={booking.id} updateRefreshBookingList={updateRefreshBookingList}/>)}
            <Pagination paging={paging} onPageSizeChange={onPageSizeChange} onPrevious={onPrevious} onNext={onNext} total={bookings.totalCount}/>
        </BookingWrapper>
  )
}

export default BookingsList
