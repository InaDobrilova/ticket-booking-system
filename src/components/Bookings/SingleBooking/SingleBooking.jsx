import React from 'react'
import { deleteBooking } from '../../../requests/bookings'
import styled from 'styled-components'

const Booking = styled.div`
  text-align: left;
  padding-left: 100px;
  color: #333;
  border-bottom: 1px solid #086788;
`

const Label = styled.span`
  font-weight: bold;
  color: black;
`

const DeleteButton = styled.button`
  background-color: #fff;
  color: red;
  border: 1px solid red;
  border-radius: 6px;
  width: 25%;
  height: 2em;
  cursor: pointer;
`

const SingleBooking = ({ booking, airports, updateRefreshBookingList }) => {
  const getAirportName = id => {
    const airport = airports.find(a => a.id === id)
    return airport.title
  }

  const handleDelete = () => {
    deleteBooking(booking.id)
    updateRefreshBookingList()
  }
  return (
        <Booking>
          <p><Label>Name: </Label>{booking.firstName} {booking.lastName}</p>
          <p><Label>Departure: </Label>{getAirportName(booking.departureAirportId)}</p>
          <p><Label>Destination: </Label>{getAirportName(booking.arrivalAirportId)}</p>
          <p><Label>Period: </Label>{booking.departureDate.split('T')[0]} / {booking.returnDate.split('T')[0]} </p>
          <p>
            <DeleteButton onClick={handleDelete}>Delete booking</DeleteButton>
          </p>
        </Booking>
  )
}

export default SingleBooking
