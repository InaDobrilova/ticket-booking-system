import React from 'react'
import Home from './components/Home/Home'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'

const App = () => {
  return (
    <div className="App">
      <Header/>
        <Home/>
        <Footer/>
    </div>
  )
}

export default App
