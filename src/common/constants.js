export const date = new Date()

export const TODAY = date.toISOString().substring(0, 10)
export const TOKEN = 'hKNsKuMiPtYCLCpvyuNPCGHaesZI5m'

export const PAGE_SIZE_MIN = 5

export const PAGE_SIZE_MID = 10

export const PAGE_SIZE_MAX = 20

export const DEFAULT_PAGE = 0
