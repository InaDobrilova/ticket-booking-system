import { DEFAULT_PAGE, PAGE_SIZE_MIN, TOKEN } from '../common/constants'

export const bookTicket = (ticketInfo) => {
  return fetch(`https://vm-fe-interview-task.herokuapp.com/api/bookings/create?authToken=${TOKEN}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      firstName: ticketInfo.firstName,
      lastName: ticketInfo.lastName,
      departureAirportId: ticketInfo.departureAirport,
      arrivalAirportId: ticketInfo.destinationAirport,
      departureDate: formatDate(new Date(ticketInfo.departureDate)),
      returnDate: formatDate(new Date(ticketInfo.returnDate))
    })
  })
    .then(r => r.json())
}

export const getAllBookings = (pageSize = PAGE_SIZE_MIN, pageIndex = DEFAULT_PAGE) => {
  return fetch(`https://vm-fe-interview-task.herokuapp.com/api/bookings?authToken=${TOKEN}&pageIndex=${pageIndex}&pageSize=${pageSize}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(r => r.json())
}

export const deleteBooking = (bookingId) => {
  return fetch(`https://vm-fe-interview-task.herokuapp.com/api/bookings/delete/${bookingId}?authToken=${TOKEN}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  })
}

const formatDate = (date) => {
  return `${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}-${date.getFullYear()}`
}
