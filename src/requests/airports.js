export const getAirports = () => {
  return fetch('https://vm-fe-interview-task.herokuapp.com/api/airports', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(r => r.json())
}
